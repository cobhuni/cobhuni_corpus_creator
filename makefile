
#
# paths
#

DATA_PATH=../../data

XML_PATH=$(DATA_PATH)/xml
OCRED_INPUT=$(XML_PATH)/ocred_texts
ALTAFSIR_INPUT=$(XML_PATH)/altafsir
HADITH_INPUT=$(XML_PATH)/hadith_alislam

ANNIS_PATH=$(DATA_PATH)/annis
OCRED_OUTPUT=$(ANNIS_PATH)/ocred_texts
ALTAFSIR_OUTPUT=$(ANNIS_PATH)/altafsir
HADITH_OUTPUT=$(ANNIS_PATH)/hadith_alislam

#
# commands
#

RM=/bin/rm -f
PYTHON=/usr/bin/env python

#
# instructions
#

.PHONY : all convert_xml convert_annis clean help

all: clean convert_xml convert_annis

convert_xml:
	$(PYTHON) json_to_xml_ocred.py
	$(PYTHON) json_to_xml_altafsir.py
	$(PYTHON) json_to_xml_hadith.py

clean:
	$(RM) $(OCRED_INPUT)/*.xml
	$(RM) $(ALTAFSIR_INPUT)/*.xml
	$(RM) $(HADITH_INPUT)/*.xml

	$(RM) $(OCRED_OUTPUT)/*
	$(RM) $(ALTAFSIR_OUTPUT)/*
	$(RM) $(HADITH_OUTPUT)/*
	
	cp ocred_texts.meta ../../data/xml/ocred_texts/
	cp altafsir.meta ../../data/xml/altafsir/
	cp hadith_alislam.meta ../../data/xml/hadith_alislam/

convert_annis:
	bash convert_workflows.sh

help:
	@echo "    clean"
	@echo "        Remove intermediate and final data"
	@echo "    convert_xml"
	@echo "        Convert annotated json files into xml"
	@echo "    convert_annis"
	@echo "        Convert xml files into annis"
	@echo "    all"
	@echo "        Clean resources and execute conversions"
