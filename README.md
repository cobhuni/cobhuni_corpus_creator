COBHUNI Corpus Creator
======================

Create annis database with COBHUNI annotated corpus along with its metadata. The output data can be found in *COBHUNI/development/corpus/visualization/cobhuni_corpus/data/annis/*. There are three subcorpora:

* ocred_texts: ocr-ed material containing hadiths.
* altafsir: extract from http://altafsir.com.
* hadith_alislam: extract from hadith.al-islam.com.

## Usage

```sh
$ make
```

## Webpage

https://www.cobhuni.uni-hamburg.de/
