#!/usr/bin/env python
#
#     util.py
#
# Utility functions
#
#################################################

import os
import sys
import itertools
from bs4 import BeautifulSoup
from operator import itemgetter
from configparser import ConfigParser


def load_config():
    """ Load configuration file

    Returns:
        <class 'configparser.ConfigParser'>: Object read from configuration file.

    """
    config = ConfigParser(inline_comment_prefixes=('#'))
    config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini'))

    return config


def get_files(path):
    """ Generate list of file paths and names from directory path.

    Args:
       path (str): Input directory

    Yields:
        str,str: full path of file and name

    """
    fobjs = (f for f in os.scandir(path) if f.is_file())

    for f in fobjs:
        yield (f.path, os.path.splitext(f.name)[0])


#FIXME nasty temporal solution
def remove_overlaps(struct):
    """ Remove annotation instances from struct that do overlap the previous annotation.

    Args:
        struct (list): Sequence of annotation instances sorted by start offsets.
            [{'val': str, 'ini': int, 'end': int, 'type': MOTIVE|METAMOT|ERROR|PERSON"},
             {'val': str, 'ini': int, 'end': int, 'type': MOTIVE|METAMOT|ERROR|PERSON"},
             ... ]

    Yields:
        dict: Sequence of non overlapping annotation instances.

    """
    config = load_config()
    
    prev_end = struct[0][config.get('json attr', 'annotation end')]

    yield struct[0]

    for ann in struct:
        current_ini = ann[config.get('json attr', 'annotation start')]

        # there is not an overlap
        if not current_ini <= prev_end:
            
            prev_end = ann[config.get('json attr', 'annotation end')]
            yield ann



def get_annotations(jsonObj):
    """ Merge all annotation instances in one list sorted by offset location.

    Args:
        jsonObjs (dict): COBHUNI document in json structure:
            {
              "content": str,
              "motives": [
                {
                  "val": str,
                  "ini": int,
                  "end": int,
                },
                {...},
                ...
              ],
              "metamotives": [...],
              "errors": [...],
              "persons": [...],
            }

    Returns:  
        list: Ordered list of annotation instances. Each item has the structure:
            { 'label' : "motive|metamotive|error|person",
              'val'   : str,
              'ini'   : int,
              'end'   : int }
    
    """
    config = load_config()

    JSON_MOTIVE = config.get('json attr','motive')
    JSON_METAMOTIVE = config.get('json attr','metamotive')
    JSON_ERROR = config.get('json attr','error')
    JSON_PERSON = config.get('json attr','person')

    ANNIS_MOTIVE = config.get('annis attr','motive')
    ANNIS_METAMOTIVE = config.get('annis attr','metamotive')
    ANNIS_ERROR = config.get('annis attr','error')
    ANNIS_PERSON = config.get('annis attr','person')

    # gather all annotation instancea and add type info
    annotation = itertools.chain(({**i, 'label' : ANNIS_MOTIVE} for i in jsonObj[JSON_MOTIVE]),
                                 ({**i, 'label' : ANNIS_METAMOTIVE} for i in jsonObj[JSON_METAMOTIVE]),
                                 ({**i, 'label' : ANNIS_ERROR} for i in jsonObj[JSON_ERROR]),
                                 ({**i, 'label' : ANNIS_PERSON} for i in jsonObj[JSON_PERSON]))

    return list(remove_overlaps(sorted(annotation, key=itemgetter(config.get('json attr', 'annotation start')))))


def create_annotated_text(soup, tag, text, annotations):
    """ Create xml tags with its text for each annotation instance and join them with the whole text.
    
    Args:
        soup (bs4.BeautifulSoup): BeautifulSoup for creating new tags.
        tag (str): Container to add text with annotations.
        text (str): Complete text.
        annotations (list): Sequence of dict containing annotation instances.

    """
    config = load_config()

    ANNIS_MOTIVE = config.get('annis attr','motive')
    ANNIS_METAMOTIVE = config.get('annis attr','metamotive')
    ANNIS_ERROR = config.get('annis attr','error')
    ANNIS_PERSON = config.get('annis attr','person')

    tag.append(text[:annotations[0]['ini']])
    size = len(annotations)
    
    for i, ann in enumerate(annotations):

        if ann['label']==ANNIS_MOTIVE:
            n = soup.new_tag('n', motive=ann['val'])
        elif ann['label']==ANNIS_METAMOTIVE:
            n = soup.new_tag('n', metamotive=ann['val'])
        elif ann['label']==ANNIS_ERROR:
            n = soup.new_tag('n', error=ann['val'])
        elif ann['label']==ANNIS_PERSON:
            n = soup.new_tag('n', person=ann['val'])
        else:
            print('Fatal Error: label "%s" not recognized in annotation structure' % ann['label'], file=sys.stderr)
            sys.exit(1)

        n.append(text[ann['ini'] : ann['end']])
        tag.append(n)

        # add text between annotations
        tag.append(text[ann['end'] : (annotations[i+1]['ini'] if i<size-1 else None)])
