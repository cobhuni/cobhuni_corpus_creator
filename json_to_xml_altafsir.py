#!/usr/bin/env python
#
#     json_to_xml_altafsir.py
#
# Get COBHUNI json annotated files from altafsir and convert text, annotation and metadata into xml format
#
# usage:
#   $ python json_to_xml_altafsir.py
#
###########################################################################################################

import os
import sys
import ujson as json
import argparse
from bs4 import BeautifulSoup
from configparser import ConfigParser

import util


#
# Constants
#

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

# load config variables
config = util.load_config()

ALTAFSIR_INPATH = os.path.join(CURRENT_PATH, config.get('json paths', 'altafsir files'))
MADHAB_NAMES_FILE = os.path.join(CURRENT_PATH, config.get('altafsir references', 'madhab names file'))
TAFSIR_NAMES_FILE = os.path.join(CURRENT_PATH, config.get('altafsir references', 'tafsir names file'))
ALTAFSIR_OUTPATH = os.path.join(CURRENT_PATH, config.get('xml paths', 'altafsir files'))

TEXT_KEY = config.get('json attr', 'text')


#
# Functions
#

def create_altafsir_xml(metadata, text, ann):
    """ Create xml structure for altafsir texts and metadata.

    Args:
        metadata (dict): Metadata to add to xml structure.
        text (str): Content to add to xml file.
        ann (list): Ordered list of annotation instances. Each item has the structure:
            { 'label' : "motive|metamotive|error|person",
              'val'   : str,
              'ini'   : int,
              'end'   : int }

    Returns:
       str: Xml string.

    """
    soup = BeautifulSoup(features='xml')

    soup.append(soup.new_tag('a'))
    soup.a.append(soup.new_tag('b', madhab=metadata['madhab'], href=metadata['url']))
    soup.a.b.append(soup.new_tag('c', tafsir=metadata['tafsir'], author=metadata['author'], date=metadata['date']))
    soup.a.b.c.append(soup.new_tag('d', sura=metadata['sura']))
    soup.a.b.c.d.append(soup.new_tag('e', aya_start=metadata['aya_start'], aya_end=metadata['aya_end']))

    soup.a.append(soup.new_tag('content'))
    
    util.create_annotated_text(soup, soup.a.content, text, ann)

    return soup.prettify()


#
# Main
#

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='convert source json files into xml')
    args = parser.parse_args()

    print('\n====== Converting files from altafsir...\n', file=sys.stderr)

    with open(MADHAB_NAMES_FILE) as fp:
        lines = (li.split('|') for li in filter(None, (l.strip() for l in fp)) if not li.startswith('#'))
        madhab_mapping = {_id.strip() : name.strip() for _id, name in lines}

    with open(TAFSIR_NAMES_FILE) as fp:
        lines = (list(map(str.strip, li.split('|'))) for li in filter(None, (l.strip() for l in fp)) if not li.startswith('#'))
        tafsir_mapping = {_id : {'name':name, 'author':author, 'date':date} for _id, name, author, date in lines}

    for fpath,fname in util.get_files(ALTAFSIR_INPATH):

        # get metadata from name
        metadata = dict(zip(('madhab', 'tafsir', 'sura', 'aya_start', 'aya_end'), fname.partition('.')[0].split('-')[1:]))

        metadata['url'] = 'http://altafsir.com/Tafasir.asp?tMadhNo=%s&tTafsirNo=%s&tSoraNo=%s&tAyahNo=%s&tDisplay=yes&LanguageID=1' % \
                               (metadata['madhab'], metadata['tafsir'], metadata['sura'], metadata['aya_start'])

        # update metadata with text info
        metadata['madhab'] = madhab_mapping[metadata['madhab']]
        metadata['author'] = tafsir_mapping[metadata['tafsir']]['author']
        metadata['date'] = tafsir_mapping[metadata['tafsir']]['date']
        metadata['tafsir'] = tafsir_mapping[metadata['tafsir']]['name']

        with open(fpath) as fp:
            
            print('**', fname, file=sys.stderr)
            jsonObj = json.load(fp)
            text = jsonObj[TEXT_KEY]
            annotations = util.get_annotations(jsonObj)

        with open(os.path.join(ALTAFSIR_OUTPATH, '%s.xml' % fname), 'w') as outfp:
            print(create_altafsir_xml(metadata, text, annotations), file=outfp)

